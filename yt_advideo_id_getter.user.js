// ==UserScript==
// @name         YT AD Video ID Getter
// @namespace    https://www.youtube.com/videoidcopy
// @version      0.1.5
// @description  A userscript that extracts the Video IDs of YouTube Ads
// @author       Philipp
// @match        https://www.youtube.com/*
// @grant        none
// ==/UserScript==

(function() {
    "use strict"

    const COPY_TEXT = "\uD83D\uDCCB"
    const COPY_SUCCESS_TEXT = "\u2705"

    const styles = [
        `
            #pps-ad-inject {
                font-size: 13px;
                pointer-events: all;
                color: #f00;

                display: inline-flex;
                flex-direction: row;
                align-items: center;
            }
        `,`
            #pps-ad-inject.ad-type-regular {
                margin-left: 10px;
            }
        `,`
            #pps-ad-inject.ad-type-shorts {
                order: -1;
                margin-bottom: 10px;
            }
        `,`
            .pps-ad-inject-link {
                color: #ff0 !important;
            }
        `,`
            .pps-ad-inject-copy {
                margin-left: 5px;
                font-size: 15px;
            }
        `
    ]

    const styleSheet = new CSSStyleSheet()
    styles.forEach(style => styleSheet.insertRule(style))
    document.adoptedStyleSheets.push(styleSheet)

    setInterval(() => {
        const [adType, injectLocation] = findAd()

        if(injectLocation && !injectLocation.querySelector("#pps-ad-inject")) {
            let adId

            try {
                adId = getAdVideoId(adType)
            } catch (e) {}

            injectAdDescription(injectLocation, adType, adId)
        }
    }, 250)

    function injectAdDescription(injectLocation, adType, adId) {
        const root = document.createElement("div")
        root.id = "pps-ad-inject"
        root.className = "ad-type-" + adType

        if(adId) {
            const adUrl = "https://youtu.be/" + adId

            const link = document.createElement("a")
            const copyBtn = document.createElement("a")

            link.className = "pps-ad-inject-link"
            copyBtn.className = "pps-ad-inject-copy"

            root.appendChild(link)
            root.appendChild(copyBtn)

            link.textContent = adId
            link.target = "_blank"
            link.href = adUrl

            copyBtn.textContent = COPY_TEXT
            copyBtn.href = "#"
            copyBtn.onclick = e => {
                e.preventDefault()

                navigator.clipboard.writeText(adUrl)

                copyBtn.textContent = COPY_SUCCESS_TEXT
                setTimeout(() => {
                    copyBtn.textContent = COPY_TEXT
                }, 750)
            }
        } else {
            root.textContent = "Can't get Video ID"
        }

        injectLocation.appendChild(root)
    }

    function findAd() {
        let ad

        if(ad = findRegularAdVideo()) {
            return ["regular", ad]
        } else if(ad = findShortsAdVideo()) {
            return ["shorts", ad]
        }

        return [null, null]
    }

    function findRegularAdVideo() {
        return document.querySelector(".videoAdUiBottomBarText") ||
            document.querySelector(".ytp-ad-player-overlay-instream-info") ||
            document.querySelector(".ytp-ad-player-overlay-layout__ad-info-container")
    }

    function findShortsAdVideo() {
        return document.querySelector("ytd-reel-video-renderer[is-active] .ytwReelsPlayerOverlayLayoutViewModelShow .ytwReelsAdCardButtonedViewModelHost") ||
            document.querySelector("ytd-reel-video-renderer[is-active] .ytwReelsPlayerOverlayLayoutViewModelShow .ytAdMetadataShapeHost")
    }

    function getAdVideoId(type="regular") {
        const playerId = type === "shorts" ? "#shorts-player" : "#movie_player"

        const debugText = document.querySelector(playerId).getDebugText()
        const json = JSON.parse(debugText)

        const videoId = type === "shorts" ? json.debug_videoId : json.addebug_videoId

        return videoId
    }

})()
